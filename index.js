//In JS, classes can be created using the "class" keyword and {}.
//Naming convention for classes: Begin with Uppercase characters

/*
	Syntax:

		class <Name> {
		
		}

*/



/*
class Student {
	constructor(name, email, grades) {
		//propertyName = value
		this.name = name;
		this.email = email;
		//add this property to the constructor
		this.gradeAve = undefined;

		if(grades.length === 4){
		     if(grades.every(grade => grade >= 0 && grade <= 100)){
		         this.grades = grades;
		     } else {
		         this.grades = undefined;
		     }
		 } else {
		     this.grades = undefined;
		 }
	}

	login() {
		console.log(`${this.email} has logged in`);
		return this;
	}
	logout() {
		console.log(`${this.email} has logged out`);
		return this
	}
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
		return this;
	}
	computeAve() {
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		//update property
		this.gradeAve = sum/4;
		//return object
		return this;
	}
	willPass() {
		return this.computeAve() >= 85 ? true : false;
	}

	willPassWithHonors() {
		if(this.willPass()) {
			if(this.computeAve() >= 90) {
				return true;
			} else {
				return false;
			}
		} else {
			return undefined;
		}
	}

}

//getter and setter
//Best practice dictates that we regulate access to such properties. We do so, via the use of "getters"(regulates retrieval) and setter (regulates manipulation).

//Method Chaining~

//Instantiation - process of creating objects from class
//To create an object from a class, use the "new" keyword, when a class has a constructor, we need to supply ALL the values needed by the constructor
let studentOne = new Student('john', 'john@mail.com', [89, 84, 78, 88]);
//let studentOne = new Student('john', 'john@mail.com', [101, 84, 78, 88]);
//let studentOne = new Student('john', 'john@mail.com', [-10, 84, 78, 88]);
//let studentOne = new Student('john', 'john@mail.com', ['hello', 84, 78, 88]);
//let studentOne = new Student('john', 'john@mail.com', [84, 78, 88]);
console.log(studentOne);
let studentTwo = new Student('Joe', 'joe@mail.com', [78, 82, 79, 85]);
let studentThree = new Student('Jane', 'jane@mail.com', [87, 89, 91, 93]);
let studentFour = new Student('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);
console.log(studentTwo);
console.log(studentThree);
console.log(studentFour);



/*
	Mini-Exercise: 6:45PM
		Create a new class called Person.

		This Person class should be able to instantiate a new object with the ff fields:

		name,
		age, (should be a number and must be a greater than or equal to 18, otherwise, set the property to undefined)
		nationality,
		address
	
		Instantiate 2 new objects from the Person class as person1 and person2

		Log both objects in the console. Take a screenshot of your console and send it to our groupchat.

*/

/*
class Person {
	constructor(name, age, nationality, address) {
		this.name = name;
		this.nationality = nationality;
		this.address = address;
		if (typeof age === "number" && age >=18) {
			this.age = age;
		} else {
			this.age = undefined;
		}
	}

}

let person1 = new Person("Wednesday Addams", 16, "American", "LA");
let person2 = new Person("Enid Sinclair", 18, "American", "San Francisco");

 console.log(person2);
 */
// console.log(person1);

// class Dog {

// }

// let dog1 = new Dog();
// console.log(dog1);

/*
Quiz:

1. What is the blueprint where objects are created from?
	Answer: Class

2. What is the naming convention applied to classes?
	Answer: upper-case first letter

3. What keyword do we use to create objects from a class?
	Answer: new

4. What is the technical term for creating an object from a class?
	Answer: instantiation

5. What class method dictates HOW objects will be created from that class?
	Answer: constructor
*/


//studentOne.logout().computeAve();

// Part 2 of activity
/*
#1 No

#2 No

#3 Yes

#4 Class Methods

#5 this
*/

class Student {
	constructor(name, email, grades) {
		//propertyName = value
		this.name = name;
		this.email = email;
		//add this property to the constructor
		this.gradeAve = undefined;
		this.passed = undefined;
		this.passedWithHonors = undefined;

		if(grades.length === 4){
		     if(grades.every(grade => grade >= 0 && grade <= 100)){
		         this.grades = grades;
		     } else {
		         this.grades = undefined;
		     }
		 } else {
		     this.grades = undefined;
		 }
	}

	login() {
		console.log(`${this.email} has logged in`);
		return this;
	}
	logout() {
		console.log(`${this.email} has logged out`);
		return this
	}
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
		return this;
	}
	computeAve() {
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		//update property
		this.gradeAve = sum/4;
		//return object
		return this;
	}
	willPass() {
		this.passed = this.computeAve() >= 85 ? true : false;
		return this;
	}

	willPassWithHonors() {
		this.passedWithHonors = this.computeAve() >=90 ? true : false;
		return this;
	}

}

let studentOne = new Student('John', 'john@mail.com', [89, 84, 78, 88]);

let studentTwo = new Student('Joe', 'joe@mail.com', [78, 82, 79, 85]);

let studentThree = new Student('Jane', 'jane@mail.com', [87, 89, 91, 93]);

let studentFour = new Student('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);
